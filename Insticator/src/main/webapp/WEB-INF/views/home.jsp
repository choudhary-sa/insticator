<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false"%>
<html>
<head>
<title>Home</title>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
	integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
	crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css"
	integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r"
	crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
	integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
	crossorigin="anonymous"></script>

</head>
<body>

	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" id="brand" href="#"> Employee Management
				</a><br> <br>
			</div>
		</div>
	</nav>



	<div class="container">
		<div class="row">
			<h1>Hello world!</h1>

			<P>The time on the server is ${serverTime}.</P>
		</div>
		<div class="row">
		<div class="col-lg-12"><font color="green">${requestScope.successMessage }</font></div>
		<div class="col-lg-12"><font color="red">${requestScope.errorMessage }</font></div>
		
			<div class="col-lg-6">

				<div class="panel-group">
					<div class="panel panel-success">
						<div class="panel-heading">
							<h4 class="panel-title">
								<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>

								<a data-toggle="collapse" href="#collapse4">Create Employee</a>
							</h4>
						</div>
						<div id="collapse4" class="panel-collapse collapse">
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12">
										<a
											href="${pageContext.request.contextPath}/createFullTimeEmployee.htm">Create
											Full Time Employee</a>
									</div>

									<div class="col-lg-12">
										<a
											href="${pageContext.request.contextPath}/createPartTimeEmployee.htm">Create
											Part Time Employee</a>
									</div>

									<div class="col-lg-12">
										<a href="${pageContext.request.contextPath}/createIntern.htm">Create
											Intern</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-6">
				<!-- <a href="${pageContext.request.contextPath}/getEditEmployee.htm">Get/Edit	Employee</a> -->

				<div class="panel-group">
					<div class="panel panel-success">
						<div class="panel-heading">
							<h4 class="panel-title">
								<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>

								<a data-toggle="collapse" href="#collapse1">View/Edit
									Employee</a>
							</h4>
						</div>
						<div id="collapse1" class="panel-collapse collapse">
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12">
										Edit Full Time Employee
										<form method="post" action="viewEditFullTime.htm">
											<input type="text" required="required" name="personId"
												pattern="^(0|[1-9][0-9]*)$" /> <input type="submit"
												class="btn btn-primary" value="Submit" />
											${requestScope.returnMessageFullTime}
										</form>
									</div>

									<div class="col-lg-12">
										Edit Intern
										<form method="post" action="viewEditIntern.htm">
											<input type="text" required="required" name="personId"
												pattern="^(0|[1-9][0-9]*)$" /> <input type="submit"
												class="btn btn-primary" value="Submit" />
											${requestScope.returnMessageIntern}
										</form>
									</div>

									<div class="col-lg-12">
										Edit Part Time Employee
										<form method="post" action="viewEditPartTime.htm">
											<input type="text" required="required" name="personId"
												pattern="^(0|[1-9][0-9]*)$" /> <input type="submit"
												class="btn btn-primary" value="Submit" />
											${requestScope.returnMessagePartTime}
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-6">

				<div class="panel-group">
					<div class="panel panel-success">
						<div class="panel-heading">
							<h4 class="panel-title">
								<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>

								<a data-toggle="collapse" href="#collapse3">Create Employee
									With JSON</a>
							</h4>
						</div>
						<div id="collapse3" class="panel-collapse collapse">
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12">
										Full Time Employee
										<form method="post" action="createFullTimeJSON.htm"
											method="POST">
											<input type="text" required="required" name="jsonPath" /> <input
												type="submit" class="btn btn-primary" value="Submit" />

										</form>
									</div>

									<div class="col-lg-12">
										Part Time Employee
										<form method="post" action="createPartTimeJSON.htm">
											<input type="text" required="required" name="jsonPath" /> <input
												type="submit" class="btn btn-primary" value="Submit" />

										</form>
									</div>

									<div class="col-lg-12">
										Intern
										<form method="post" action="createInternJSON.htm">
											<input type="text" required="required" name="jsonPath" /> <input
												type="submit" class="btn btn-primary" value="Submit" />

										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>
