<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Create Employee</title>

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
	integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
	crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css"
	integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r"
	crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
	integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
	crossorigin="anonymous"></script>


</head>
<body>
	<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" id="brand" href="#"> Employee Management
			</a><br> <br> <a href="${pageContext.request.contextPath}/">Home
			</a>
		</div>
	</div>
	</nav>


	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h3>Create Part Time</h3>

				<h5>${requestScope.partTimeName}</h5>
				<form:form action="createPartTimeRecord.htm" commandName="partTimeEmployee"
					method="post">
					<font color="red"><form:errors path="*" /></font>
					<br>
					<table class="table">
						<tr>
							<td>First Name:</td>
							<td><form:input type="text" path="firstName"
									name="firstName" value="" required="true" /></td>
							<td><font color="red"><form:errors path="firstName" /></font></td>

						</tr>

						<tr>
							<td>Last Name:</td>
							<td><form:input type="text" path="lastName" name="lastName"
									value="" required="true" /></td>
							<td><font color="red"><form:errors path="lastName" /></font></td>
						</tr>

						<tr>
							<td>Date of Birth:</td>
							<td><form:input type="date" path="dateOfBirth"
									name="dateOfBirth" value="" required="true" /></td>
							<td><font color="red"><form:errors path="dateOfBirth" /></font></td>
						</tr>

						<tr>

							<td>Gender:</td>
							<td><br> <form:radiobutton name="gender" value="M"
									path="gender" required="true" />M<br> <form:radiobutton
									name="gender" path="gender" value="F" />F</td>
							<td><font color="red"><form:errors path="gender" /></font></td>
						</tr>

						<tr>

							<td>Address:</td>
							<td><br> <form:textarea name="address" path="address"
									required="true" /></td>
							<td><font color="red"><form:errors path="address" /></font></td>
						</tr>

						<tr>
							<td>Date of Joining:</td>
							<td><form:input type="date" path="dateOfJoining"
									name="dateOfJoining" value="" required="true" /></td>
							<td><font color="red"><form:errors
										path="dateOfJoining" /></font></td>
						</tr>


						<tr>
							<td>SSN:</td>
							<td><form:input type="text" name="sSN" value="" path="ssn"
									required="true" /></td>
							<td><font color="red"><form:errors path="ssn" /></font></td>
						</tr>


						<tr>
							<td>Phone Number:</td>
							<td><form:input type="text" name="phoneNumber" value=""
									path="phoneNumber" required="true" /></td>
							<td><font color="red"><form:errors path="phoneNumber" /></font></td>
						</tr>



						<tr>
							<td>Email:</td>
							<td><form:input type="text" name="emailId" value=""
									path="emailId" required="true" /></td>
							<td><font color="red"><form:errors path="emailId" /></font></td>
						</tr>
						
						<tr>
							<td>Weekly Hours:</td>
							<td><input type="text" value="20" disabled /></td>
							<td></td>
						</tr>

					</table>
					<input type="submit" class="btn btn-success"
						value="Create Part Time" />
				</form:form>

			</div>
		</div>
	</div>
</body>
</html>