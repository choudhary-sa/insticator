package com.insticator.dao;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import com.insticator.pojo.PartTimeEmployee;

public class PartTimeDAO extends DAO {
	public PartTimeDAO() {

	}

	public PartTimeEmployee create(PartTimeEmployee partTimeEmployee) {

		try {
			begin();
			partTimeEmployee.setDateOfLeaving(partTimeEmployee.getDateOfJoining().plusYears(2));
			getSession().save(partTimeEmployee);
			commit();
			close();
			return partTimeEmployee;
		} catch (HibernateException he) {
			he.printStackTrace();
			rollback();
		}
		return null;

	}
	
	public PartTimeEmployee getPartTimeEmployee(long personID) {
		try {
			begin();
			PartTimeEmployee partTimeEmployee = new PartTimeEmployee();
			Query q = getSession().createQuery("from PartTimeEmployee where personID = :personID");
			q.setLong("personID", personID);
			partTimeEmployee = (PartTimeEmployee) q.uniqueResult();
			commit();
			close();
			return partTimeEmployee;
		} catch (HibernateException he) {
			he.printStackTrace();
			rollback();
			return null;
		}
	}
	
	
	public PartTimeEmployee updatePartTimeEmployee(PartTimeEmployee pe,long personId) {
		try {
			
			
			PartTimeEmployee i=getPartTimeEmployee(personId);
			begin();
			i.setFirstName(pe.getFirstName());
			i.setLastName(pe.getLastName());
			i.setDateOfBirth(pe.getDateOfBirth());
			i.setGender(pe.getGender());
			i.setAddress(pe.getAddress());
			i.setDateOfJoining(pe.getDateOfJoining());
			i.setDateOfLeaving(pe.getDateOfLeaving());
			i.setSsn(pe.getSsn());
			i.setPhoneNumber(pe.getPhoneNumber());
			i.setEmailId(pe.getEmailId());
			
			getSession().update(i);
			commit();
			close();
			return i;
		} catch (HibernateException he) {
			he.printStackTrace();
			rollback();
			return null;
		}
	}
}
