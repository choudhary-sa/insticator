package com.insticator.dao;

import org.hibernate.HibernateException;
import org.hibernate.Query;

import com.insticator.pojo.Intern;

public class InternDAO extends DAO {
	public InternDAO() {

	}

	public Intern create(Intern intern) {

		try {
			begin();
			intern.setDateOfLeaving(intern.getDateOfJoining().plusMonths(intern.getDurationInMonths()));
			getSession().save(intern);
			commit();
			close();
			return intern;
		} catch (HibernateException he) {
			he.printStackTrace();
			rollback();
		}
		return null;

	}
	
	public Intern getIntern(long personID) {
		try {
			begin();
			Intern employee = new Intern();
			Query q = getSession().createQuery("from Intern where personID = :personID");
			q.setLong("personID", personID);
			employee = (Intern) q.uniqueResult();
			commit();
			close();
			return employee;
		} catch (HibernateException he) {
			he.printStackTrace();
			rollback();
			return null;
		}
	}
	
	public Intern updateIntern(Intern intern,long personId) {
		try {
			
			
			Intern i=getIntern(personId);
			begin();
			i.setFirstName(intern.getFirstName());
			i.setLastName(intern.getLastName());
			i.setDateOfBirth(intern.getDateOfBirth());
			i.setGender(intern.getGender());
			i.setAddress(intern.getAddress());
			i.setDateOfJoining(intern.getDateOfJoining());
			i.setDateOfLeaving(intern.getDateOfLeaving());
			i.setSsn(intern.getSsn());
			i.setPhoneNumber(intern.getPhoneNumber());
			i.setEmailId(intern.getEmailId());
			i.setHourlyRate(intern.getHourlyRate());
			i.setSeason(intern.getSeason());
			i.setDurationInMonths(intern.getDurationInMonths());
			getSession().update(i);
			commit();
			close();
			return i;
		} catch (HibernateException he) {
			he.printStackTrace();
			rollback();
			return null;
		}
	}
}
