package com.insticator.dao;

import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.joda.time.LocalDate;

import com.insticator.dao.DAO;
import com.insticator.pojo.Employee;
import com.insticator.pojo.FullTimeEmployee;



public class EmployeeDAO extends DAO {

	public EmployeeDAO() {

	}

	public Employee create(FullTimeEmployee fullTimeEmployee) {

		try {
			begin();
			// FullTimeEmployee employee = new FullTimeEmployee();
			// employee.setFirstName(fullTimeEmployee.getFirstName());
			// employee.setLastName(fullTimeEmployee.getLastName());
			fullTimeEmployee.setDateOfLeaving(new LocalDate("9999-12-31"));
			getSession().save(fullTimeEmployee);
			commit();
			close();
			return fullTimeEmployee;
		} catch (HibernateException he) {
			he.printStackTrace();
			rollback();
		}
		return null;

	}

	public Employee createFromMap(Map<String, String> params) {
		System.out.println("Inside the method");
		try {
			begin();
			FullTimeEmployee employee = new FullTimeEmployee();
			employee.setFirstName(params.get("firstName"));
			employee.setLastName(params.get("lastName"));
			getSession().save(employee);
			commit();
			close();
			return employee;
		} catch (HibernateException he) {
			he.printStackTrace();
			rollback();
		}
		return null;

	}

	public FullTimeEmployee getFullTimeEmployee(long personID) {
		try {
			begin();
			FullTimeEmployee employee = new FullTimeEmployee();
			Query q = getSession().createQuery("from FullTimeEmployee where personID = :personID");
			q.setLong("personID", personID);
			employee = (FullTimeEmployee) q.uniqueResult();
			commit();
			close();
			return employee;
		} catch (HibernateException he) {
			he.printStackTrace();
			rollback();
			return null;
		}
	}
	
	public FullTimeEmployee updateFullTimeEmployee(FullTimeEmployee fe,long personId) {
		try {
			
			
			FullTimeEmployee i=getFullTimeEmployee(personId);
			begin();
			i.setFirstName(fe.getFirstName());
			i.setLastName(fe.getLastName());
			i.setDateOfBirth(fe.getDateOfBirth());
			i.setGender(fe.getGender());
			i.setAddress(fe.getAddress());
			i.setDateOfJoining(fe.getDateOfJoining());
			i.setDateOfLeaving(fe.getDateOfLeaving());
			i.setSsn(fe.getSsn());
			i.setPhoneNumber(fe.getPhoneNumber());
			i.setEmailId(fe.getEmailId());
			i.setDesignation(fe.getDesignation());
			i.setPayRateType(fe.getPayRateType());
			i.setEmployeeBenifits(fe.getEmployeeBenifits());
			i.setJoiningBonus(fe.getJoiningBonus());
			i.setSalary(fe.getSalary());
			getSession().update(i);
			commit();
			close();
			return fe;
		} catch (HibernateException he) {
			he.printStackTrace();
			rollback();
			return null;
		}
	}
}
