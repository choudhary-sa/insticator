package com.insticator.solution;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.insticator.pojo.FullTimeEmployee;
import com.insticator.pojo.Intern;
import com.insticator.pojo.PartTimeEmployee;


/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	@Autowired
	@Qualifier("fullTimeEmployee")
	FullTimeEmployee fullTimeEmployee;
	
	@Autowired
	@Qualifier("partTimeEmployee")
	PartTimeEmployee partTimeEmployee;
	
	@Autowired
	@Qualifier("intern")
	Intern intern;
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "home";
	}
	
//	@RequestMapping(value = "/", method = RequestMethod.POST)
//	public String homePost(Locale locale, Model model) {
//		home(locale, model);
//
//		return "home";
//	}
	
	//createFullTimeEmployee
	@RequestMapping(value = "/createFullTimeEmployee.htm", method = RequestMethod.GET)
	public String createFullTimeEmployee(Locale locale, Model model) {
		

		model.addAttribute("fullTimeEmployee", fullTimeEmployee);
		return "createFullTimeEmployee";
	}
	
	//createPartTimeEmployee
	@RequestMapping(value = "/createPartTimeEmployee.htm", method = RequestMethod.GET)
	public String createPartTimeEmployee(Locale locale, Model model) {
		
		model.addAttribute("partTimeEmployee", partTimeEmployee);
		return "createPartTimeEmployee";
	}
	
	//createIntern
	@RequestMapping(value = "/createIntern.htm", method = RequestMethod.GET)
	public String createIntern(Locale locale, Model model) {
		
		model.addAttribute("intern", intern);
		return "createIntern";
	}
	
	@RequestMapping(value = "/getEditEmployee.htm", method = RequestMethod.GET)
	public String getEditEmployee(Locale locale, Model model) {
		

		return "getEditEmployee";
	}
	
}
