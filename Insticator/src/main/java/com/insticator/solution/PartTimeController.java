package com.insticator.solution;

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.insticator.dao.PartTimeDAO;

import com.insticator.pojo.PartTimeEmployee;

@Controller
public class PartTimeController {
	
	@Autowired
	@Qualifier("partTimeDAO")
	PartTimeDAO partTimeDAO;
	
	//createPartTimeRecord.htm
	@RequestMapping(value = "/createPartTimeRecord.htm", method = RequestMethod.POST)
	public String createPartTimeRecord(Locale locale, @Valid @ModelAttribute("partTimeEmployee") PartTimeEmployee partTimeEmployee, BindingResult result,HttpServletRequest req,
			HttpServletResponse res,@RequestParam Map<String, String> params) {
		
		System.out.println("Inside Controller");
		if(result.hasErrors()){
			return "createPartTimeEmployee";
		}
					
		partTimeDAO.create(partTimeEmployee);
		req.setAttribute("partTimeName", partTimeEmployee.getFirstName()+" "+partTimeEmployee.getLastName()+" Created");
		return "createPartTimeEmployee";
	}
	
	@RequestMapping(value = "/viewEditPartTime.htm", method = RequestMethod.POST)
	public String viewEditPartTime(Locale locale, HttpServletRequest req,
			HttpServletResponse res,Model model) {
		
		System.out.println("Inside Controller");
		
		PartTimeEmployee e=partTimeDAO.getPartTimeEmployee(Integer.parseInt(req.getParameter("personId")));
		if(null==e){
			req.setAttribute("returnMessagePartTime", "Employee record does not exist.");
			return "home";
		}
		else{
			model.addAttribute("partTimeEmployee", e);
			req.setAttribute("personId", e.getPersonID());
			return "viewEditEmployee/viewEditPartTime";
		}
	}
	
	//viewEditPartimeRecord.htm
		@RequestMapping(value = "/viewEditPartimeRecord.htm", method = RequestMethod.POST)
		public String viewEditPartimeRecord(Locale locale, @Valid @ModelAttribute("partTimeEmployee") PartTimeEmployee partTimeEmployee, BindingResult result,HttpServletRequest req,
				HttpServletResponse res,@RequestParam Map<String, String> params) {
			
			System.out.println("Inside Controller");
			if(result.hasErrors()){
				return "viewEditEmployee/viewEditPartTime";
			}
			System.out.println("Person Id "+Long.parseLong(req.getParameter("personId")));
			
			
			long personId=Long.parseLong(req.getParameter("personId"));
			partTimeDAO.updatePartTimeEmployee(partTimeEmployee,personId);
			req.setAttribute("successMessage", partTimeEmployee.getFirstName()+" "+partTimeEmployee.getLastName()+" updated");
			return "home";
		}
		
		//saveAsJsonPartTime.htm
		@RequestMapping(value = "/saveAsJsonPartTime.htm", method = RequestMethod.GET)
		public String saveAsJson(Locale locale, @Valid @ModelAttribute("partTimeEmployee") PartTimeEmployee partTimeEmployee, BindingResult result,HttpServletRequest req,
				HttpServletResponse res,@RequestParam Map<String, String> params){
			
			ObjectMapper mapper = new ObjectMapper();
			try {
				int personId=Integer.parseInt(req.getParameter("personId"));
				partTimeEmployee=partTimeDAO.getPartTimeEmployee(personId);
				// Convert object to JSON string and save into a file directly
				mapper.writeValue(new File("C:\\json\\"+partTimeEmployee.getFirstName()+partTimeEmployee.getPersonID()), partTimeEmployee);

				// Convert object to JSON string and pretty print
				
				
				String jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(partTimeEmployee);
				System.out.println(jsonInString);
				req.setAttribute("successMessage", "Saved as JSON");
				return "home";

			} catch (JsonGenerationException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return "home";

		}
		
		//createPartTimeJSON
				@RequestMapping(value = "/createPartTimeJSON.htm", method = RequestMethod.POST)
				public String createPartTimeJSON(Locale locale, HttpServletRequest req,
						HttpServletResponse res,Model model/*,@RequestParam("jsonPath") MultipartFile file*/) {
					
					System.out.println("Inside Controller");
					String path=req.getParameter("jsonPath");
					ObjectMapper mapper = new ObjectMapper();
					mapper.registerModule(new JodaModule());
					try{
						// Convert JSON string from file to Object
						PartTimeEmployee employee = mapper.readValue(new File("C:\\readFromJson\\"+path), PartTimeEmployee.class);
						System.out.println(employee.getFirstName()+" "+employee.getLastName());
						model.addAttribute("partTimeEmployee", employee);
						return "createPartTimeEmployee";
					}catch (JsonGenerationException e) {
						e.printStackTrace();
						req.setAttribute("errorMessage", "You have proided in valid data in JSON file");
						return "home";
					} catch (JsonMappingException e) {
						e.printStackTrace();
						req.setAttribute("errorMessage", "You have proided in valid data in JSON file");
						return "home";
					} catch (IOException e) {
						e.printStackTrace();
						req.setAttribute("errorMessage", "You have proided in valid data in JSON file");
						return "home";
					}
					

					
				}
}
