package com.insticator.solution;

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.insticator.dao.EmployeeDAO;
import com.insticator.pojo.FullTimeEmployee;
import com.insticator.validator.FullTimeValidator;


@Controller
public class EmployeeController {

	@Autowired
	@Qualifier("employeeDAO")
	EmployeeDAO employeeDAO;
	
	@Autowired
	@Qualifier("fullTimeValidator")
	FullTimeValidator fullTimeValidator;
	
	//createFullTimeEmployee
	@RequestMapping(value = "/createFullTimeEmployeeRecord.htm", method = RequestMethod.POST)
	public String createFullTimeEmployee(Locale locale, @Valid @ModelAttribute("fullTimeEmployee") FullTimeEmployee fullTimeEmployee, BindingResult result,HttpServletRequest req,
			HttpServletResponse res,@RequestParam Map<String, String> params) {
		
		System.out.println("Inside Controller");
		if(result.hasErrors()){
			return "createFullTimeEmployee";
		}
		
		System.out.println("Inside Controller 1");
		employeeDAO.create(fullTimeEmployee);
		//employeeDAO.createFromMap(params);
		
		req.setAttribute("employee", fullTimeEmployee.getFirstName()+" "+fullTimeEmployee.getLastName()+" Created");
		return "createFullTimeEmployee";
	}
	
	//viewEditFullTime
	@RequestMapping(value = "/viewEditFullTime.htm", method = RequestMethod.POST)
	public String viewEditFullTime(Locale locale, HttpServletRequest req,
			HttpServletResponse res,Model model) {
		
		System.out.println("Inside Controller");
		
		FullTimeEmployee e=employeeDAO.getFullTimeEmployee(Integer.parseInt(req.getParameter("personId")));
		if(null==e){
			req.setAttribute("returnMessageFullTime", "Employee record does not exist.");
			return "home";
		}
		else{
			model.addAttribute("fullTimeEmployee", e);
			req.setAttribute("personId", e.getPersonID());
			return "viewEditEmployee/viewEditFullTimeEmployee";
		}
		
	}
	
	//viewEditFullTimeEmployeeRecord.htm
		@RequestMapping(value = "/viewEditFullTimeEmployeeRecord.htm", method = RequestMethod.POST)
		public String viewEditFullTimeEmployeeRecord(Locale locale, @Valid @ModelAttribute("fullTimeEmployee") FullTimeEmployee fullTimeEmployee, BindingResult result,HttpServletRequest req,
				HttpServletResponse res,@RequestParam Map<String, String> params) {
			
			System.out.println("Inside Controller");
			if(result.hasErrors()){
				return "viewEditEmployee/viewEditFullTimeEmployeeRecord";
			}
			System.out.println("Person Id "+Long.parseLong(req.getParameter("personId")));
			
			
			long personId=Long.parseLong(req.getParameter("personId"));
			employeeDAO.updateFullTimeEmployee(fullTimeEmployee,personId);
			req.setAttribute("successMessage", fullTimeEmployee.getFirstName()+" "+fullTimeEmployee.getLastName()+" updated");
			return "home";
		}
		
	//createFullTimeJSON
	@RequestMapping(value = "/createFullTimeJSON.htm", method = RequestMethod.POST)
	public String createFullTimeJSON(Locale locale, HttpServletRequest req,
			HttpServletResponse res,Model model/*,@RequestParam("jsonPath") MultipartFile file*/) {
		
		System.out.println("Inside Controller");
		String path=req.getParameter("jsonPath");
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JodaModule());
		try{
			// Convert JSON string from file to Object
			FullTimeEmployee employee = mapper.readValue(new File("C:\\readFromJson\\"+path), FullTimeEmployee.class);
			System.out.println(employee.getFirstName()+" "+employee.getLastName());
			model.addAttribute("fullTimeEmployee", employee);
			return "createFullTimeEmployee";
		}catch (JsonGenerationException e) {
			e.printStackTrace();
			req.setAttribute("errorMessage", "You have proided in valid data in JSON file");
			return "home";
		} catch (JsonMappingException e) {
			e.printStackTrace();
			req.setAttribute("errorMessage", "You have proided in valid data in JSON file");
			return "home";
		} catch (IOException e) {
			e.printStackTrace();
			req.setAttribute("errorMessage", "You have proided in valid data in JSON file");
			return "home";
		}
		

		
	}
	
	@RequestMapping(value = "/saveAsJsonFullTime.htm", method = RequestMethod.GET)
	public String saveAsJson(Locale locale, @Valid @ModelAttribute("fullTimeEmployee") FullTimeEmployee fullTimeEmployee, BindingResult result,HttpServletRequest req,
			HttpServletResponse res,@RequestParam Map<String, String> params){
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			int personId=Integer.parseInt(req.getParameter("personId"));
			fullTimeEmployee=employeeDAO.getFullTimeEmployee(personId);
			// Convert object to JSON string and save into a file directly
			mapper.writeValue(new File("C:\\json\\"+fullTimeEmployee.getFirstName()+fullTimeEmployee.getPersonID()), fullTimeEmployee);

			// Convert object to JSON string and pretty print
			
			
			String jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(fullTimeEmployee);
			System.out.println(jsonInString);
			req.setAttribute("successMessage", "Saved as JSON");
			return "home";

		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "home";

	}
}
