package com.insticator.solution;

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.insticator.dao.InternDAO;

import com.insticator.pojo.Intern;

@Controller
public class InternController {
	//InternDAO
	@Autowired
	@Qualifier("internDAO")
	InternDAO internDAO;
	
	
	//createInternRecord
	@RequestMapping(value = "/createInternRecord.htm", method = RequestMethod.POST)
	public String createIntern(Locale locale, @Valid @ModelAttribute("intern") Intern intern, BindingResult result,HttpServletRequest req,
			HttpServletResponse res,@RequestParam Map<String, String> params) {
		
		System.out.println("Inside Controller");
		if(result.hasErrors()){
			return "createIntern";
		}
		
		System.out.println("Inside Controller 1");
		
		internDAO.create(intern);
		req.setAttribute("internName", intern.getFirstName()+" "+intern.getLastName()+" Created");
		return "createIntern";
	}
	
	@RequestMapping(value = "/viewEditIntern.htm", method = RequestMethod.POST)
	public String viewEditFullTime(Locale locale, HttpServletRequest req,
			HttpServletResponse res,Model model) {
		
		System.out.println("Inside Controller");
		
		Intern e=internDAO.getIntern(Integer.parseInt(req.getParameter("personId")));
		if(null==e){
			req.setAttribute("returnMessageIntern", "Employee record does not exist.");
			return "home";
		}
		else{
			model.addAttribute("intern", e);
			req.setAttribute("personId", e.getPersonID());
			return "viewEditEmployee/viewEditIntern";
		}
		
	}
	
	//viewEditInternRecord.htm
	@RequestMapping(value = "/viewEditInternRecord.htm", method = RequestMethod.POST)
	public String viewEditInternRecord(Locale locale, @Valid @ModelAttribute("intern") Intern intern, BindingResult result,HttpServletRequest req,
			HttpServletResponse res,@RequestParam Map<String, String> params) {
		
		System.out.println("Inside Controller");
		if(result.hasErrors()){
			return "viewEditEmployee/viewEditIntern";
		}
		System.out.println("Person Id "+Long.parseLong(req.getParameter("personId")));
		
		
		long personId=Long.parseLong(req.getParameter("personId"));
		internDAO.updateIntern(intern,personId);
		req.setAttribute("successMessage", intern.getFirstName()+" "+intern.getLastName()+" updated");
		return "home";
	}
	
	@RequestMapping(value = "/saveAsJsonIntern.htm", method = RequestMethod.GET)
	public String saveAsJson(Locale locale, @Valid @ModelAttribute("intern") Intern intern, BindingResult result,HttpServletRequest req,
			HttpServletResponse res,@RequestParam Map<String, String> params){
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			int personId=Integer.parseInt(req.getParameter("personId"));
			intern=internDAO.getIntern(personId);
			// Convert object to JSON string and save into a file directly
			mapper.writeValue(new File("C:\\json\\"+intern.getFirstName()+intern.getPersonID()), intern);

			// Convert object to JSON string and pretty print
			
			
			String jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(intern);
			System.out.println(jsonInString);
			req.setAttribute("successMessage", "Saved as JSON");
			return "home";

		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "home";

	}
	
	//createInternJSON
		@RequestMapping(value = "/createInternJSON.htm", method = RequestMethod.POST)
		public String createInternJSON(Locale locale, HttpServletRequest req,
				HttpServletResponse res,Model model/*,@RequestParam("jsonPath") MultipartFile file*/) {
			
			System.out.println("Inside Controller");
			String path=req.getParameter("jsonPath");
			ObjectMapper mapper = new ObjectMapper();
			mapper.registerModule(new JodaModule());
			try{
				// Convert JSON string from file to Object
				Intern employee = mapper.readValue(new File("C:\\readFromJson\\"+path), Intern.class);
				System.out.println(employee.getFirstName()+" "+employee.getLastName());
				model.addAttribute("intern", employee);
				return "createIntern";
			}catch (JsonGenerationException e) {
				e.printStackTrace();
				req.setAttribute("errorMessage", "You have proided in valid data in JSON file");
				return "home";
			} catch (JsonMappingException e) {
				e.printStackTrace();
				req.setAttribute("errorMessage", "You have proided in valid data in JSON file");
				return "home";
			} catch (IOException e) {
				e.printStackTrace();
				req.setAttribute("errorMessage", "You have proided in valid data in JSON file");
				return "home";
			}
			

			
		}
}
