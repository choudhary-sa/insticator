package com.insticator.pojo;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "fulltimeemployee")
@Inheritance(strategy = InheritanceType.JOINED)
//@PrimaryKeyJoinColumn(name = "personID")
public class FullTimeEmployee extends Employee{

	@NotNull
	@Column(name="salary")
	@Min(value=1,message="Salary cannot be 0 or -ve")
	private float salary;
	
	@NotNull
	@Column(name="designation")
	private String designation;
	
	@NotNull(message="Pay rate not null")
	@Column(name="payRateType")
	private String payRateType;
	
	@ElementCollection(fetch=FetchType.EAGER)
	@Column(name="employeeBenifits")
	private Set<String> employeeBenifits;
	
	@NotNull
	@Min(value=0,message="Enter valid value")
	private float joiningBonus;
	
	public float getSalary() {
		return salary;
	}
	public void setSalary(float salary) {
		this.salary = salary;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getPayRateType() {
		return payRateType;
	}
	public void setPayRateType(String payRateType) {
		this.payRateType = payRateType;
	}

	public Set<String> getEmployeeBenifits() {
		return employeeBenifits;
	}
	public void setEmployeeBenifits(Set<String> employeeBenifits) {
		this.employeeBenifits = employeeBenifits;
	}
	public float getJoiningBonus() {
		return joiningBonus;
	}
	public void setJoiningBonus(float joiningBonus) {
		this.joiningBonus = joiningBonus;
	}
	
}
