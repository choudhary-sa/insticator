package com.insticator.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "parttimeemployee")
@Inheritance(strategy = InheritanceType.JOINED)
public class PartTimeEmployee extends Employee {
	
	@Column(name="weeklyhours")
	private float weeklyHours=20;

	public float getWeeklyHours() {
		return weeklyHours;
	}

	public void setWeeklyHours(float weeklyHours) {
		this.weeklyHours = weeklyHours;
	}
	
	
}
