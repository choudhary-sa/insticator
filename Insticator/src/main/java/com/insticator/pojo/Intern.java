package com.insticator.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "intern")
@Inheritance(strategy = InheritanceType.JOINED)
//@PrimaryKeyJoinColumn(name = "personID")
public class Intern extends Employee{

	@NotNull(message="Enter hourly rate")
	@Column(name="hourlyRate")
	@Min(value=15,message="Minimum salary for intern is 15 and maximum is 40")
	@Max(value=40,message="Minimum salary for intern is 15 and maximum is 40")
	private float hourlyRate;
	
	@NotNull(message="Enter Duration of internship")
	@Min(value=3,message="Minimum duration is 3 months and maximum is 8 months")
	@Max(value=8,message="Minimum duration is 3 months and maximum is 8 months")
	@Column(name="durationInMonths")
	private int durationInMonths;
	
	public int getDurationInMonths() {
		return durationInMonths;
	}
	public void setDurationInMonths(int durationInMonths) {
		this.durationInMonths = durationInMonths;
	}
	@NotNull(message="Provide starting season of internship")
	@Column(name="season")
	private String season;
	
	public float getHourlyRate() {
		return hourlyRate;
	}
	public void setHourlyRate(float hourlyRate) {
		this.hourlyRate = hourlyRate;
	}
	public String getSeason() {
		return season;
	}
	public void setSeason(String season) {
		this.season = season;
	}
	
	
}
