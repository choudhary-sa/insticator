package com.insticator.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Email;
import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "employee")
@Inheritance(strategy = InheritanceType.JOINED)
//@PrimaryKeyJoinColumn(name = "personID")
public class Employee extends Person{

	@Column(name="ssn")
	@NotNull
	@Pattern(regexp="^(\\d{3}-?\\d{2}-?\\d{4}|XXX-XX-XXXX)$",message="Please enter a valid SSN number")
	private String ssn;	
	
	@NotNull
	@Column(name="address")
	private String address;
	
	@Column(name="phoneNumber")
	@Digits(integer=10,fraction=0,message="Please enter a valid phone number of 10 digits")
	private long phoneNumber;
	
	public long getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	@Future
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name="dateOfJoining")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate dateOfJoining;
	
	@Future
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name="dateOfLeaving")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	private LocalDate dateOfLeaving;
	
	@NotNull
	@Email
	@Column(name = "emailid")
	private String emailId;
	
	
	public LocalDate getDateOfJoining() {
		return dateOfJoining;
	}
	public void setDateOfJoining(LocalDate dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}
	public LocalDate getDateOfLeaving() {
		return dateOfLeaving;
	}
	public void setDateOfLeaving(LocalDate dateOfLeaving) {
		this.dateOfLeaving = dateOfLeaving;
	}
	public String getSsn() {
		return ssn;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	
	
	
}
